This documentation is for Windows systems.

**Installation**

1-	Install Matlab 2021a. Older versions won’t work.

2-	Install ROS Toolbox. 

3-	Follow instructions in https://www.mathworks.com/help/ros/gs/ros-system-requirements.html#mw_582ba4f4-dda1-4b95-a6f5-b639669aff56

4-	Follow the tutorial for custom messages: https://www.mathworks.com/help/ros/ug/ros-2-custom-message-support.html

5-	Download repositories that contain message type definitions, and collect them in a folder. 

Even if some of these messages are standard for Ros2 Dashing, matlab doesn’t add them during the initial ros toolbox installation. 


### [Deep Orange Msgs](https://gitlab.com/cuicardeeporange/iac_deep_orange_msgs/-tree/master/deep_orange_msgs)

### [Csn Msgs](https://github.com/nobleo/can_msgs)

### [Automotive Autonomy Msgs](https://github.com/astuff/automotive_autonomy_msgs)

### [Autoware Auto Msgs](https://gitlab.com/autowarefoundation/autoware.auto/autoware_auto_msgs/-/tree/master/autoware_auto_msgs)

### [Geometry Msgs](https://github.com/ros/geometry2)

### [GPS Msgs](https://github.com/swri-robotics/gps_umd/tree/dashing-devel)

### [Novatel Msgs](https://github.com/swri-robotics/novatel_gps_driver/tree/dashing-devel/novatel_gps_driver)

### [Raptor dbw Msgs](https://gitlab.com/IACBaseSoftware/raptor-dbw-ros2)


5- Autoware auto repository uses .idl messaeg files to define message types, however, Matlab can only build with .msg files. You can find the folder /autoware_auto_msgs that contains the messag files that are converted to .msg format. Replace the autoware_auto_msgs folder with the original repository folder.

6- After collecting all packages in your preferred folder, run defineMessageTypes.m. Matlab will build the ros packages.


**Usage**

1- Take a ros2bag. Folder name that contains ros2bag must be same with the name of the .db3 file name.

2- Run listTopics.m to see what topics are inside (only ones that you receive messages).   "/parameter_events"     "/tf_static"     "/target_path" can not be parsed currently. 

3- Run generateCsvFieldNames.m to generate a csv file that lists all the fields on all the topics. 

4- Run parseDesiredFields.m to read the list from csv file, parse the messages into one vector for the desired variable names and save the log struct to a .mat file. 

5- You can customize your .csv file to parse only specific topics & fields with your desired field names in the log struct. 

