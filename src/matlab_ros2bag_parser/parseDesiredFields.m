clearvars -except bag name

name='20210723_steering_identification2_0';
% name='20210706_lor_path_server_ontrack_01_0';

% bag=ros2bag([pwd name]); 
tic
table=readtable(['fieldList_' name '.csv'])

log=struct;
readNextTopic=true;
for i =1:height(table)
   
    
    
    tic
    if readNextTopic==true
        disp(['reading topic ' num2str(i)])
        topic=select(bag,'Topic',string(table{i,1}) );

        topicRead= readMessages(topic);
        msgReadTime=toc
    end
    log.(string(table{i,3}))=zeros(length(topicRead),1);
    

    currData=eval("cellfun(@(m) (m."+(string(table{i,2}))+" ),topicRead,'UniformOutput',false)");
    if ~isempty(currData)
        if ischar(currData{1})
            eval("log.(string(table{i,3})) = string(currData) "+";");
        else
            eval("log.(string(table{i,3})) = cell2mat(currData)"+";");

        end
     log.( strcat(string(table{i,3}),'_time' ) ) = topic.MessageList.Time;

    disp(strcat( ' parsed field, ' ,num2str(i) ,' out of ', num2str(height(table)) ) )
    end
    if i<height(table)
        if string(table{i+1,1})~=string(table{i,1})
            readNextTopic=true;
        else
            readNextTopic=false;
        end
    end
    
    
 end
save(['parsedFields_' name],'log')
totalTime=toc
