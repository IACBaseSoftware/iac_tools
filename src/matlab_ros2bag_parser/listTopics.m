clearvars -except bag
% name='20210629_lor_ontrack_03_camera_0';
% name='20200720_run6_0';
name='20210723_steering_identification2_0';

 bag=ros2bag([pwd name]); %autonomous lat+lon
%  bag=ros2bag(['E:\' name]); %autonomous lat+lon

msgReceivedIdx =  find(bag.AvailableTopics{:,1} ~= 0 );

allTopicNames=string( bag.AvailableTopics(msgReceivedIdx,:).Properties.RowNames);

notWantedTopicNames = ["/parameter_events";...
                       "/tf_static";...
                       "/target_path"];

% wantedTopicNames = ["/raptor_dbw_interface/steering_cmd"; ...
%                     "/raptor_dbw_interface/steering_report" ;
%                     "/joystick/steering_cmd"];

wantedTopicNames = allTopicNames;
                
for k =1:length(notWantedTopicNames)
    
notWantedIdx(k)=find(strcmp(wantedTopicNames,notWantedTopicNames(k) ))
end

wantedTopicNames(notWantedIdx)=[];

receivedTopicNames = strrep(wantedTopicNames, '/','A');

disp(  [ num2str( (1:length( receivedTopicNames))')  +  receivedTopicNames ]);
log = struct;
topicList=struct;


for i =1:length(allTopicNames)
    topic=select(bag,'Topic',wantedTopicNames(i));
    tic
    topicRead= readMessages( topic);
    topicList.(receivedTopicNames(i))=topicRead{1,1};
    toc
end

save(['topicList_' name],'topicList')