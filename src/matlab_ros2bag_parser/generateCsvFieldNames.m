clearvars -except bag name
clc
% name='20210706_lor_path_server_ontrack_01_0';
import java.util.Stack; 
load(['topicList_' name]);
% load('topicList_20210629_lor_ontrack_03_camera_0')
stack = getNames(topicList);

list = strings(stack.size(),1);
values = Stack();
i = 1;
while stack.size() > 0
    list(i) = stack.pop();
     i = i + 1;
end

list = strrep(list,'A','/');
for i=1:length(list)
names= strsplit(list(i),'.');
topics(i)=names(1);
fields(i)=strjoin(names(2:end),'.');
end
topics=['Topics' topics]';
fields=['Fields' fields]';

desiredNames=list;

dm1 = strrep(desiredNames, '/','_');
[dm2, dm3] = strtok( dm1, '_' );
desiredNames = strcat( dm2, dm3 );
desiredNames = strrep(desiredNames, '.','_');
desiredNames = strrep(desiredNames, '(','_');
desiredNames = strrep(desiredNames, ')','_');

desiredNames = ['Desired Names'; desiredNames];

writematrix([topics fields desiredNames],['fieldList_' name '.csv'])

function names = getNames(struct)
import java.util.Stack; 

structs = fieldnames(struct);
names = Stack();

for n = 1:numel(structs)
   b = cell2mat(structs(n));
   if isa(struct.(b),'struct')
        internal = getNames(struct.(b));
        while internal.size() > 0
            pop = internal.pop();
            if length(struct.(b)) > 1
                for i=1:length(struct.(b))
                    c = b + "(" + num2str(i) + ")." + pop;
                    names.push(c);
                end
            else
                c = b + "." + pop;
                names.push(c);
            end
        end
   else
            names.push(b);
   end
end

end